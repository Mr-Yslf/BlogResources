### 仓库项目说明

`BDMap`   -- [Qt嵌入百度地图API的详细流程与常见问题](https://blog.csdn.net/qq_34578785/article/details/106671018)

`Trail` -- [百度地图API绘制轨迹](https://blog.csdn.net/qq_34578785/article/details/105301266)

`MultiThreadCamera` --

[Qt5.7 + OpenCV3.2开启多线程调用系统摄像头并实现视频录制与回放、图片截取与保存(一)本地图片的读取与显示](https://blog.csdn.net/qq_34578785/article/details/102962399)

[Qt5.7 + OpenCV3.2开启多线程调用系统摄像头并实现视频录制与回放、图片截取与保存(二)摄像头画面显示与视频保存](https://blog.csdn.net/qq_34578785/article/details/103040085)

[Qt5.7 + OpenCV3.2开启多线程调用系统摄像头并实现视频录制与回放、图片截取与保存(三)多线程实现](https://blog.csdn.net/qq_34578785/article/details/105990410)

`Position_Translate` -- [百度坐标(BD09)、GCJ02、与WGS84之间的转换 C/C++ UI实现](https://blog.csdn.net/qq_34578785/article/details/102991954)

`TCP` -- [Qt网络编程之TCP通信（一）聊天窗口的实现](https://blog.csdn.net/qq_34578785/article/details/106937328)

`TCPFile` -- [Qt网络编程之TCP通信（二）文件传输](https://blog.csdn.net/qq_34578785/article/details/106992357)

`UdpMsg` -- [Qt网络编程之UDP通信（一）聊天窗口的实现](https://blog.csdn.net/qq_34578785/article/details/107033686)

`UdpVideo` -- [Qt网络编程之UDP通信（二）视频传输](https://blog.csdn.net/qq_34578785/article/details/107080908)

